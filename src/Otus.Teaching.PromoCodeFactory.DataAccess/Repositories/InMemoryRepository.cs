﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryRepository<T>
        : IRepository<T>
        where T : BaseEntity, ICloneable
    {
        protected List<T> Data { get; set; }

        public InMemoryRepository(IEnumerable<T> data)
        {
            Data = data.ToList();
        }

        public Task<IEnumerable<T>> GetAllAsync()
        {
            return Task.FromResult(Data.AsEnumerable());
        }

        public Task<T> GetByIdAsync(Guid id)
        {
            return Task.FromResult(Data.FirstOrDefault(x => x.Id == id));
        }

        public Task<T> CreateAsync(T entity)
        {
            Data.Add((T)entity.Clone());
            return Task.FromResult(entity);
        }

        public Task<T> UpdateAsync(T entity)
        {
            var item = Data.FirstOrDefault(x => x.Id == entity.Id);
            if (item != null)
            {
                var index = Data.IndexOf(item);
                Data[index] = (T)entity.Clone();
                return Task.FromResult(Data[index]);
            }
            throw new InvalidOperationException("Entity with given id not found");
        }

        public Task DeleteAsync(Guid id)
        {
            var item = Data.FirstOrDefault(x => x.Id == id);
            if (item != null)
            {
                Data.Remove(item);
                return Task.CompletedTask;
            }
            throw new InvalidOperationException("Entity with given id not found");
        }
    }

}